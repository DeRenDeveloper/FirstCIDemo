//
//  FirstDemoTests.swift
//  FirstDemoTests
//
//  Created by 吳得人 on 2017/6/8.
//  Copyright © 2017年 吳得人. All rights reserved.
//

import XCTest
@testable import FirstDemo

class FirstDemoTests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        super.setUp()
        viewController = ViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_NumberOfVowels_WhenPassDominik_ReturnsThree() {
        let viewController = ViewController()
        let string = "Dominik"
        let numberOfVowels = viewController.numberOfVowels(string: string)
        
        XCTAssertEqual(numberOfVowels, 3, "should find 3 vowels in Dominik", file: "FirstDemoTest.swift", line: 24)
    }
    
    func test_MakeHeadline_ReturnsStringWithEachWordStartCapital() {

        let input = "this is a test headline"
        let exceptOutput = "This Is A Test Headline"
        
        let headline = viewController.makeHeadline(from: input)
        
        XCTAssertEqual(headline, exceptOutput)
        
    }
    
    func test_MakeHeadline_ReturnsStringWithEachWordStartCapital2() {
        
        let input = "Here is another Example"
        let exceptOutput = "Here Is Another Example"
        
        let headline = viewController.makeHeadline(from: input)
        
        XCTAssertEqual(headline, exceptOutput)
        
    }
    
    
//    func testExample() {
//        // This is an example of a functional test case.
//        // Use XCTAssert and related functions to verify your tests produce the correct results.
//    }
//    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
