//
//  ViewController.swift
//  FirstDemo
//
//  Created by 吳得人 on 2017/6/8.
//  Copyright © 2017年 吳得人. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    func numberOfVowels(string: String) -> Int {
        let vowels: [Character] = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
        return string.characters.reduce(0) {
            $0 + (vowels.contains($1) ? 1 : 0)
        }
    //
//        var numberOfVowels = 0
//        for character in string.characters {
//            if vowels.contains(character) {
//                numberOfVowels += 1
//            }
//        }
//        return numberOfVowels
    }
    
    func makeHeadline(from string: String) -> String {
        
        let words = string.components(separatedBy: " ")
        
        let headlineWords = words.map{ (word) -> String in
        
            var mutableWord = word
            let first = mutableWord.remove(at: mutableWord.startIndex)
            
            return String(first).uppercased() + mutableWord
        }
        
        return headlineWords.joined(separator:" ")
    }
    
    var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.layer.borderColor = UIColor.yellow.cgColor
        picker.translatesAutoresizingMaskIntoConstraints = false
        return picker
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(pickerView)
        pickerView.dataSource = self as? UIPickerViewDataSource
        pickerView.delegate = self as? UIPickerViewDelegate
        pickerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 64).isActive = true
        pickerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        pickerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pickerView.widthAnchor.constraint(equalToConstant: 64).isActive = true
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

